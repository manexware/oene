# -*- coding: utf-8 -*-
{
    'name': "OpenEduNav Behavior",

    'summary': """
        Módulo para la calificación de la conducta de los estudiantes
        de los cursos impartidos por ESCAPE.
        """,

    'description': """
        Módulo para la calificación de la conducta de los estudiantes
        de los cursos impartidos por ESCAPE.
    """,

    'author': "DIGEDO",
    'website': "http://siedigedo.armada.mil.ec",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Educacion',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['openedunav_escape'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'menu/behavior_menu.xml',

        'views/views.xml',
        'views/news_views.xml',
        'views/templates.xml',
        'views/license_views.xml',
        'views/brigadier_note_views.xml',
        'views/report_behavior_view.xml',
        'views/aspects_views.xml',
        'report/report_behavior_py3o.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}