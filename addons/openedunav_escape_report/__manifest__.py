# -*- coding: utf-8 -*-
{
    'name': 'OpenEduNav ESCAPE Reports',
    'summary': """Special reports""",
    'author': 'Manexware, S.A.',
    'website': 'http://www.manexware.com',
    'category': 'Reports',
    'version': '0.1',
    'depends': [
        'openedunav_escape'
    ],
    'data': [
        # 'wizard/professional_attitude_wizard_view.xml',
        # 'wizard/productivity_wizard_view.xml',
        # 'wizard/productivity_student_wizard_view.xml',
        # 'wizard/integrator_product_wizard_view.xml',
        # 'wizard/integrator_product_student_wizard_view.xml',
        # 'wizard/score_wizard_view.xml',
        # 'wizard/score_student_wizard_view.xml',
        # 'wizard/score_professor_wizard_view.xml',
        # 'wizard/certificate_wizard_view.xml',
        # 'wizard/subject_wizard_view.xml',
        # 'wizard/professional_attitude_professor_wizard_view.xml',
        # 'wizard/professional_attitude_student_wizard_view.xml',
        # 'menu/sie_report_menu.xml',
        # 'main.xml',
        # 'views/report_score_act.xml',
        # 'views/report_subject_act.xml',
        # 'report/score_act_layout.xml',
        'report/certificado_evaluacion.xml',
        'wizard/certificate_evaluation_wizard_view.xml',
        'menu/sie_report_menu.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
