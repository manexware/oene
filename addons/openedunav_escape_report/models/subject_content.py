# -*- coding: utf-8 -*


import time
import re
import logging

from odoo import models, fields, api, _
from datetime import datetime
import locale
import pytz


class SieSubjectContent(models.Model):
    _inherit = 'sie.subject.content'
    _description = 'Subject Content'

    def get_total_hours(self):
        total_hours = 0
        for content in self:
            total_hours += content.hours
        return total_hours

    def get_format_date(self):
        if date:
            locale.setlocale(locale.LC_TIME, "es_EC.utf-8")
            fecha = datetime.strptime(date, '%Y-%m-%d').strftime('%d de %B del %Y')
            return unicode(fecha.decode('UTF-8'))

    def current_date(self):
        my_tz = pytz.timezone(self._context.get('tz') or 'UTC')
        return datetime.now(my_tz).strftime('Guayaquil, %d de %m/ del %Y')

    def get_register_director(self, course):
        return self.env['sie.register.directors'].search([('course_id','=',course.id)], limit=1)

    def get_statistician_name(self):
        register_director = self.get_register_director(self[0].subject_id.course_id)
        return register_director.statistician_id.full_name

    def get_statistician_grade(self):
        register_director = self.get_register_director(self[0].subject_id.course_id)
        return register_director.statistician_id.get_grade()

    def get_statistician_position(self):
        register_director = self.get_register_director(self[0].subject_id.course_id)
        return register_director.position_statistician
    
    def get_director_name(self):
        register_director = self.get_register_director(self[0].subject_id.course_id)
        return register_director.director_id.full_name

    def get_director_grade(self):
        register_director = self.get_register_director(self[0].subject_id.course_id)
        return register_director.director_id.get_grade()

    def get_director_position(self):
        register_director = self.get_register_director(self[0].subject_id.course_id)
        return register_director.position_director


