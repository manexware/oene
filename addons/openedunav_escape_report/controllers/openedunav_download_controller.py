# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.http import route, request, Controller
from odoo.tools import html_escape
from odoo.addons.report.controllers.main import ReportController
from odoo.addons.web.controllers.main import _serialize_exception, content_disposition
from odoo.tools.safe_eval import safe_eval as eval
import time

import json
from werkzeug import exceptions, url_decode
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse
from werkzeug.datastructures import Headers


class OpenedunavDownloadController(ReportController):
    @route(['/report/download'])
    def report_download(self, data, token):
        response = super(OpenedunavDownloadController, self).report_download(data, token)
        # if we got another content disposition before, ditch the one added
        # by super()
        requestcontent = json.loads(data)
        url, type = requestcontent[0], requestcontent[1]
        try:
            if type == 'qweb-pdf':
                reportname = url.split('/report/pdf/')[1].split('?')[0]
                docids = None
                if '/' in reportname:
                    reportname, docids = reportname.split('/')
                if not docids:
                    cr, uid = request.cr, request.uid
                    report = request.registry['report']._get_report_from_name(cr, uid, reportname)
                    data = dict(url_decode(url.split('?')[1]).items())
                    data = json.loads(data.pop('options'))
                    response.headers['Content-Disposition'] = content_disposition(data['download_name'])
                    return response
        except Exception, e:
            se = _serialize_exception(e)
            error = {
                'code': 200,
                'message': "Odoo Server Error",
                'data': se
            }
            return request.make_response(html_escape(json.dumps(error)))
        return response
