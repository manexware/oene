#!/usr/bin/env python
#  -*- coding: UTF-8 -*-

from operator import attrgetter
from odoo import http
from . import podunk_report as report
import decimal
from . import util as util
from . import report_base


class AttitudeReport(report_base.ReportBase):
    def __init__(self, local_tz, local_dt, subject_id, course_id, parameter_id,
                 evaluator_id, student_id, order):
        report_base.ReportBase.__init__(self, local_tz, local_dt, subject_id, course_id, parameter_id,
                                            evaluator_id, None, None, None, None, student_id, order)

    def get_detail_professional_attitudes(self):
        course = http.request.env['sie.course'].search([('id', '=', int(self.course_id))])
        course_name = course.course_name.name
        promotion_name = '%s %s %s ' % (course.promotion_course.name, u'AÑO:', course.year)
        parameter = self.get_parameter_by_id(self.parameter_id)
        coefficient = parameter.coefficient
        parameter_code = parameter.param_name.code
        full_path = self.directory + 'actitud_profesional.pdf'
        evaluator_name = ''
        filename = ''
        date_time = self.local_tz.normalize(self.local_dt).strftime("%d-%b-%Y_%H-%M-%S")
        if self.evaluator_id != 'False':
            evaluator = http.request.env['sie.faculty'].search([('id', '=', int(self.evaluator_id))])
            evaluator_name = evaluator.display_title_name
        if parameter_code == '030':
            filename = '%s_%s_coevaluacion_%s%s' % (self.get_initials_course(course_name), 'AP', date_time, '.pdf')
            full_path = self.directory + filename
            attitudes = self.get_professional_attitudes(self.course_id, self.parameter_id)
            if attitudes:
                scores_array = self.get_scores_array_coe(attitudes, coefficient)
            else:
                scores_array = []
            full_title = 'COEVALUACIÓN:  OFICIALES ALUMNOS'
            title_report = 'Informe de Notas Detalladas de Actitud Profesional'
            report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id), course_name,
                              promotion_name,
                              title_report, full_title)
        elif parameter_code == '028':
            filename = '%s_%s_oficiales_planta_%s_%s%s' % (self.get_initials_course(course_name), 'AP', date_time,
                                                           evaluator_name.replace(' ', '_'), '.pdf')
            full_path = self.directory + filename
            attitudes = self.get_professional_attitudes(self.course_id, self.parameter_id, self.evaluator_id)
            if attitudes:
                scores_array = self.get_scores_array_pla(attitudes, coefficient)
                scores_array = self.get_scores_array_without_prom(scores_array)
            else:
                scores_array = []
            full_title = 'OFICIAL DE PLANTA: %s' % evaluator_name
            title_report = 'Informe de Notas Detalladas de Actitud Profesional'

            report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id), course_name,
                              promotion_name,
                              title_report, full_title)
        elif parameter_code == '027':
            filename = '%s_%s_oficiales_division_%s_%s%s' % (self.get_initials_course(course_name), 'AP', date_time,
                                                             evaluator_name.replace(' ', '_'), '.pdf')
            full_path = self.directory + filename
            attitudes = self.get_professional_attitudes(self.course_id, self.parameter_id, self.evaluator_id)
            if attitudes:
                scores_array = self.get_scores_array_div(attitudes, coefficient)
                scores_array = self.get_scores_array_without_prom(scores_array)
            else:
                scores_array = []
            full_title = 'OFICIAL DE DIVISION: %s' % evaluator_name
            title_report = 'Informe de Notas Detalladas de Actitud Profesional'
            report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id), course_name,
                              promotion_name,
                              title_report, full_title)
        elif parameter_code == '026':
            filename = '%s_%s_director_%s_%s%s' % (self.get_initials_course(course_name), 'AP', date_time,
                                                   evaluator_name.replace(' ', '_'), '.pdf')
            full_path = self.directory + filename
            attitudes = self.get_professional_attitudes(self.course_id, self.parameter_id, self.evaluator_id)
            if attitudes:
                scores_array = self.get_scores_array_dir(attitudes, coefficient)
                scores_array = self.get_scores_array_without_prom(scores_array)
            else:
                scores_array = []
            full_title = 'DIRECTOR: %s' % evaluator_name
            title_report = 'Informe de Notas Detalladas de Actitud Profesional'
            report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id), course_name,
                              promotion_name,
                              title_report, full_title)
        elif parameter_code == '029':
            teacher_id = self.evaluator_id
            teacher_name = ''
            if self.subject_id != 'False':
                subject = http.request.env['sie.subject'].search([('id', '=', int(self.subject_id))])
                teacher_id = subject.faculty_id[0].id
                teacher_name = subject.faculty_id[0].display_name
            filename = '%s_%s_profesor_%s_%s%s' % (self.get_initials_course(course_name), 'AP', date_time,
                                                   teacher_name.replace(' ', '_'), '.pdf')
            full_path = self.directory + filename
            attitudes = self.get_professional_attitudes(self.course_id, self.parameter_id, teacher_id, self.subject_id)
            if attitudes:
                scores_array = self.get_scores_array_pro(attitudes, coefficient)
            else:
                scores_array = []
            full_title = 'PROFESOR: %s' % teacher_name
            title_report = 'Informe de Notas Detalladas de Actitud Profesional'
            report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id), course_name,
                              promotion_name,
                              title_report, full_title)
        return filename, full_path

    def get_summary_professional_attitudes(self):
        date_time = self.local_tz.normalize(self.local_dt).strftime("%d-%b-%Y_%H-%M-%S")
        course = http.request.env['sie.course'].search([('id', '=', int(self.course_id))])
        course_name = course.course_name.name
        filename = '%s_actitud-profesional_sumarizadas_%s%s' % (self.get_initials_course(course_name), date_time,
                                                                '.pdf')
        full_path = self.directory + filename

        promotion_name = '%s %s %s ' % (course.promotion_course.name, u'AÑO:', course.year)

        scores_array = self.get_summary_professional_attitudes_array(course)
        if scores_array:
            if self.order == "promedio":
                index = len(scores_array[0]) - 2
                scores_array = sorted(scores_array,
                                      key=lambda nota: float(nota[index]) if nota[index] != "A.P." else 30,
                                      reverse=True)
                scores_array = self.re_number(scores_array)
            data = util.get_elements(scores_array)
            media = util.mean(data)
            deviation = util.pstdev(data)
            if media != "---":
                media = util.format_sie(util.round_sie(media, self.get_sie_digits()), self.get_sie_digits())
            if deviation != "---":
                deviation = util.format_sie(util.round_sie(deviation, self.get_sie_digits()),
                                            self.get_sie_digits())
        else:
            scores_array = []
            media = ''
            deviation = ''
        title_report = 'Informe de Notas Sumarizadas de Actitud Profesional'
        deviation_title = 'Promedio: %s     Desv.: %s' % (media.replace('.', ','), deviation.replace('.', ','))
        report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id), course_name,
                          promotion_name,
                          title_report, None, None, None, None, None, None, deviation_title)
        return filename, full_path
