#!/usr/bin/env python
#  -*- coding: UTF-8 -*-

from odoo import http
from . import podunk_report as report
import decimal
from . import util as util
from . import report_base


class ProductivityReport(report_base.ReportBase):
    def __init__(self, local_tz, local_dt, course_id, parameter_id, direction_work_id, student_id, order):
        report_base.ReportBase.__init__(self, local_tz, local_dt, None, course_id, parameter_id, None,
                                            direction_work_id,
                                            None,
                                            None,
                                            None,
                                            student_id,
                                            order)

    def get_detail_productivity(self):
        full_path = self.directory + 'productividad_detallada.pdf'
        course = http.request.env['sie.course'].search([('id', '=', int(self.course_id))])
        course_name = course.course_name.name
        matrix_id = str(course.matrix_id.id)
        promotion_name = '%s %s %s ' % (course.promotion_course.name, u'AÑO:', course.year)
        filename = ''
        date_time = self.local_tz.normalize(self.local_dt).strftime("%d-%b-%Y_%H-%M-%S")
        parameter = self.get_parameter_by_id(self.parameter_id)
        param_name_code = parameter.param_name.code

        if self.student_id:
            student = self.get_student(self.student_id)
        else:
            student = None

        if param_name_code == '023':
            filename = '%s_CONF_%s%s' % (self.get_initials_course(course_name), date_time, '.pdf')
            full_path = self.directory + filename
            scores_array = self.get_detail_con_tdi(self.parameter_id, matrix_id, 'TII')
            full_title = 'T.I.I. CONFERENCIA'
            title_report = 'Informe de Notas Detalladas de Conferencias'
            if student:
                if not student.inactive:
                    report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id),
                                      course_name, promotion_name, title_report, full_title)
            else:
                # scores_array = []
                report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, None), course_name,
                                  promotion_name, title_report, full_title)

        elif param_name_code == '024':
            work_name, scores_array = self.get_detail_tdd(self.direction_work_id, self.parameter_id, matrix_id)
            filename = '%s_TDD_%s_%s%s' % (self.get_initials_course(course_name), date_time,
                                           work_name.replace(' ', '_'), '.pdf')
            full_path = self.directory + filename
            full_title = 'Trabajo: %s' % work_name
            title_report = 'Informe de Notas Detalladas de Trabajos de Dirección'
            if student:
                if not student.inactive:
                    report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id),
                                      course_name, promotion_name, title_report, full_title)
            else:
                # scores_array = []
                report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, None), course_name,
                                  promotion_name, title_report, full_title)

        elif param_name_code == '032':
            filename = '%s_TDI_%s%s' % (self.get_initials_course(course_name), date_time, '.pdf')
            full_path = self.directory + filename
            scores_array = self.get_detail_con_tdi(self.parameter_id, matrix_id, 'TDI')
            full_title = u'Trabajo de Investigación Individual'
            title_report = 'Informe de Notas Detalladas de Trabajos de Investigación Individual'
            if student:
                if not student.inactive:
                    report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id),
                                      course_name, promotion_name, title_report, full_title)
            else:
                # scores_array = []
                report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, None), course_name,
                                  promotion_name, title_report, full_title)
        return filename, full_path

    def get_summary_productivity(self):
        date_time = self.local_tz.normalize(self.local_dt).strftime("%d-%b-%Y_%H-%M-%S")
        course = self.get_course(self.course_id)
        course_name = course.course_name.name
        filename = '%s_productividad_sumarizadas_%s%s' % (self.get_initials_course(course_name), date_time, '.pdf')

        full_path = self.directory + filename

        promotion_name = '%s %s %s ' % (course.promotion_course.name, u'AÑO:', course.year)

        scores_array = self.get_summary_productivity_array(course)
        if scores_array:
            if self.order == "promedio":
                index = len(scores_array[0]) - 2
                scores_array = sorted(scores_array,
                                      key=lambda nota: float(nota[index]) if nota[index] != "PROD." else 30,
                                      reverse=True)
                scores_array = self.re_number(scores_array)
            data = util.get_elements(scores_array)
            media = util.mean(data)
            deviation = util.pstdev(data)
            if media != "---":
                media = util.format_sie(util.round_sie(media, self.get_sie_digits()), self.get_sie_digits())
            if deviation != "---":
                deviation = util.format_sie(util.round_sie(deviation, self.get_sie_digits()),
                                            self.get_sie_digits())
        else:
            scores_array = []
            media = ''
            deviation = ''

        title_report = 'Informe de Notas Sumarizadas de Productividad'
        deviation_title = 'Promedio: %s     Desv.: %s' % (media.replace('.', ','), deviation.replace('.', ','))
        report.report_pdf(full_path, self.get_scores_array_without_id(scores_array, self.student_id), course_name,
                          promotion_name,
                          title_report, None, None, None, None, None, None, deviation_title)
        return filename, full_path

    def get_summary_productivity_course(self, course_id, param_name):
        date_time = self.local_tz.normalize(self.local_dt).strftime("%d-%b-%Y_%H-%M-%S")
        course = self.get_course(course_id)
        course_name = course.course_name.name
        filename = '%s_PI_%s_%s%s' % (self.get_initials_course(course_name), param_name.name, date_time, '.pdf')

        full_path = self.directory + filename

        promotion_name = '%s %s %s ' % (course.promotion_course.name, u'AÑO:', course.year)

        scores_array = self.get_summary_productivity_array(course)
        if scores_array:
            data = util.get_elements(scores_array)
            media = util.mean(data)
            deviation = util.pstdev(data)
            if media != "---":
                media = util.format_sie(util.round_sie(media, self.get_sie_digits()), self.get_sie_digits())
            if deviation != "---":
                deviation = util.format_sie(util.round_sie(deviation, self.get_sie_digits()),
                                            self.get_sie_digits())
        else:
            scores_array = []
            media = '---'
            deviation = '---'

        title_report = 'Informe de Notas Sumarizadas de Productividad'
        deviation_title = 'Promedio: %s     Desv.: %s' % (media.replace('.', ','), deviation.replace('.', ','))
        report.report_pdf(full_path, self.get_scores_array_without_id(scores_array), course_name, promotion_name,
                          title_report, None, None, None, None, None, None, deviation_title)
        return filename, full_path
