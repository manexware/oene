# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ProfessionalAttitudeProfessorWizard(models.TransientModel):
    _name = 'report.professional.attitude.professor.wizard'
    _description = 'Report Attitude Professional Professor'

    course_id = fields.Many2one('sie.course', string='Course', required=True)
    parameter_id = fields.Many2one('sie.matrix.parameter',
                                   domain="[('last_child', '=', True), ('parent_ref', 'like', '025'),"
                                          "('course_ref', '=',matrix_id)]", required=True)
    subject_id = fields.Many2one('sie.subject', string='Subject', ondelete='restrict',
                                 domain="[('faculty_id.user_id', '=', uid),('course_id', '=', course_id)]")
    param_name_code = fields.Char(string='Param name code', compute='_compute_param_name_code', store=True)
    matrix_id = fields.Char(compute="_compute_matrix", store=True)

    # @api.one
    # @api.depends('course_id')
    # def _compute_parameter_id(self):
    #     if self.course_id:
    #         param_name = self.env['sie.param.name'].search([('code','=','029')])
    #         parameter_id = self.env['sie.matrix.parameter'].search([('course_ref','=',self.course_id.matrix_id.id),('param_name','=',param_name.id)])
    #         self.parameter_id = parameter_id.id

    @api.one
    @api.depends('parameter_id')
    def _compute_param_name_code(self):
        if self.parameter_id:
            self.param_name_code = self.parameter_id.param_name.code

    @api.one
    @api.depends('course_id')
    def _compute_matrix(self):
        if self.course_id:
            self.matrix_id = self.course_id.matrix_id.id

    @api.multi
    def print_report(self):
        if self.parameter_id.param_name.code == '030':
            return
        evaluator_id = self.env['sie.faculty'].search([('user_id', '=', self._uid)])
        url = '/web/aguena/report_attitude?' \
              '&course_id=%s' \
              '&report_type=D' \
              '&parameter_id=%s' \
              '&evaluator_id=%s' \
              '&subject_id=%s' \
              % (self.course_id.id, self.parameter_id.id, evaluator_id.id, self.subject_id.id)

        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self',
        }
