# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ProductivityWizard(models.TransientModel):
    _name = 'report.productivity.wizard'
    _description = 'Report productivity'

    course_id = fields.Many2one('sie.course', string='Course', required=True)
    # parameter_id = fields.Selection(selection=[('TRABAJOS DE DIRECCION','Direction Work'),
    #                                            ('TII - CONFERENCIAS','conference'),
    #                                            ('TRABAJOS DE INVESTIGACION INDIVIDUAL','Investigate Work')],
    #                                 string='Parameter'  )
    parameter_id = fields.Many2one('sie.matrix.parameter', string='Parameter', ondelete='restrict',
                                   domain="[('last_child', '=', False),"
                                          "('parent_ref', 'like', '022'),"
                                          "('course_ref', '=', matrix_id)]")
    # parameter_sub = fields.Selection(selection=[('NOTICIAS','News'), ('PERIODICO MURAL','Newspaper')],
    #                                  string='Parameter sub',
    #                                  default='NOTICIAS')
    matrix_id = fields.Char(compute="_compute_matrix", store=True)
    selected_report = fields.Selection(string="Type", selection=[('S', 'Summary'), ('D', 'Detail')], required=True)
    # evaluator_id = fields.Many2one('sie.course.professional.attitude',
    #                                 domain="[('evaluator', '=', True),('register_work_ids', '=', direction_work_id)]")
    direction_work_id = fields.Many2one('sie.register.work', string='Direction work',
                                        domain="[('course_id', '=', course_id), ('work_type', '=','direction')]")
    param_name_code = fields.Char(compute='_compute_param_name_code')
    ordenar = fields.Selection(string='Ordenar por', selection=[('nombre', 'Nombre'), ('promedio', 'Promedio')])

    @api.one
    @api.depends('course_id')
    def _compute_matrix(self):
        if self.course_id:
            self.matrix_id = self.course_id.matrix_id.id

    @api.multi
    @api.depends('parameter_id')
    def _compute_param_name_code(self):
        if self.parameter_id:
            self.param_name_code = self.parameter_id.param_name.code

    @api.multi
    def print_report(self):

        url = '/web/aguena/report_productivity?' \
              '&course_id=%s' \
              '&report_type=%s' \
              '&parameter_id=%s' \
              '&direction_work_id=%s' \
              '&ordenar=%s' \
              % (self.course_id.id, self.selected_report, self.parameter_id.id, self.direction_work_id.id, self.ordenar)
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self',
        }
