# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ProfessionalAttitudeWizard(models.TransientModel):
    _name = 'report.professional.attitude.wizard'
    _description = 'Report Attitude Professional'

    course_id = fields.Many2one('sie.course', string='Course', required=True)
    parameter_id = fields.Many2one('sie.matrix.parameter',
                                   domain="[('last_child', '=', True), ('parent_ref', 'like', '025'),"
                                          "('course_ref', '=',matrix_id)]")
    matrix_id = fields.Char(compute="_compute_matrix", store=True)
    selected_report = fields.Selection(string="Type", selection=[('S', 'Summary'), ('D', 'Detail')], required=True)
    evaluator_id = fields.Many2one('sie.faculty')
    param_name_code = fields.Char(string='Param name code', compute='_compute_param_name_code', store=True)
    subject_id = fields.Many2one('sie.subject', string='Subject', ondelete='restrict')
    ordenar = fields.Selection(string='Ordenar por', selection=[('nombre', 'Nombre'), ('promedio', 'Promedio')])

    @api.onchange('parameter_id')
    def onchange_place(self):
        if self.parameter_id.param_name.code == '029':
            res = {}
            subject_ids = []
            professional_attitudes = self.env['sie.professional.attitude']. \
                search([('course_id', '=', self.course_id.id), ('parameter_id', '=', self.parameter_id.id)])
            for professional_attitude in professional_attitudes:
                subject_ids.append(int(professional_attitude.subject_id.id))
            res['domain'] = {'subject_id': [('id', '=', subject_ids)]}
            return res
        elif self.parameter_id.param_name.code == '026' or self.parameter_id.param_name.code == '027' or self.parameter_id.param_name.code == '028':
            res = {}
            faculty_ids = []
            professional_attitudes = self.env['sie.professional.attitude']. \
                search([('course_id', '=', self.course_id.id), ('parameter_id', '=', self.parameter_id.id)])
            for professional_attitude in professional_attitudes:
                faculty_ids.append(int(professional_attitude.faculty_id.id))
            list(set(faculty_ids))
            res['domain'] = {'evaluator_id': [('id', '=', faculty_ids)]}
            return res
        else:
            self.evaluator_id = []

    @api.one
    @api.depends('course_id')
    def _compute_matrix(self):
        if self.course_id:
            self.matrix_id = self.course_id.matrix_id.id

    @api.one
    @api.depends('parameter_id')
    def _compute_param_name_code(self):
        if self.parameter_id:
            self.param_name_code = self.parameter_id.param_name.code

    @api.multi
    def print_report(self):
        url = '/web/aguena/report_attitude?' \
              '&course_id=%s' \
              '&report_type=%s' \
              '&parameter_id=%s' \
              '&evaluator_id=%s' \
              '&subject_id=%s' \
              '&ordenar=%s' \
              % (self.course_id.id, self.selected_report, self.parameter_id.id, self.evaluator_id.id,
                 self.subject_id.id, self.ordenar)

        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self',
        }
