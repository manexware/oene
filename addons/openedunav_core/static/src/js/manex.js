(function($) {
    //---Función de devolver la cantidad de decimales de un número

    function giveMeDecimals(number:Number):Number{

       var numberString:String = String(number);

       var numberArray:Array = numberString.split(".");

       var decimals:Number = (numberArray[1].length != undefined)? numberArray[1].length:0;

       return Math.pow(10, decimals);

    }

    //---Función de convertir un número a un string con 3 decimales
    function convert(number:Number):String{

       var stringNumber:String = "";

       switch(giveMeDecimals(number)){

          case 1:
          stringNumber = ".000";
          break;

          case 10:
          stringNumber = "00";
          break;

          case 100:
          stringNumber = "0";
          break;

       }
       return(number + stringNumber);

    }
});