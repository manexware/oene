(function() {
    var instance = openerp;
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;

    instance.web.form.FieldScore = instance.web.form.FieldChar.extend({
        render_value: function () {
            var self = this;
            this._super();
            this.$('input').inputmask('Regex', {
                //regex: "^(20|\\\d([.,]\\\d{0,3})?)$",
                regex: "^([1]?[0-9]([\\\,|.]\\\d{0,3})?|20)$",
                //regex: "^([0-1]?[0-9]|20)(\\\,|.\\\d{0,3})?$",
                onincomplete: function () {
                    self.$el.addClass('oe_form_invalid');
                },
                oncomplete: function () {
                    self.$el.removeClass('oe_form_invalid');
                },
            });
        },
    });

    instance.web.form.widgets.add('score', 'instance.web.form.FieldScore');

})();
