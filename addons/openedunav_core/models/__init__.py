# -*- coding: utf-8 -*-
##############################################################################
#
#    Manexware S.A.
#    Copyright (C) 2000-TODAY Manexware S.A.(<http://www.manexware.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from . import res_partner
from . import res_users
from . import res_company
from . import misc
from . import academic_title
from . import category
from . import grade
from . import nato
from . import promotion
from . import person_title
from . import faculty
from . import religion
from . import specialty
from . import student
from . import training_shaft
from . import res_users
from . import sub_specialty
from . import allocation

