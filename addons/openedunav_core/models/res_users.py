# -*- coding: utf-8 -*-

import logging

from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _name = "res.users"
    _inherit = "res.users"

    @api.multi
    def create_user(self, records, user_group=None):
        for rec in records:
            if not rec.user_ids:
                user_vals = {
                    'name': rec.name,
                    'login': rec.email,
                    'partner_id': rec.partner_id.id
                }
                user_id = self.create(user_vals)
                rec.user_ids = user_id
                if user_group:
                    user_group.users = user_group.users + user_id

    @api.multi
    @api.onchange('first_name', 'middle_name', 'last_name', 'mother_name')
    def _compute_name(self):
        for record in self:
            if record.first_name and record.last_name and record.mother_name:
                if record.middle_name:
                    display_name = '%s %s %s %s' % (record.last_name, record.mother_name,
                                                    record.first_name, record.middle_name)
                else:
                    display_name = '%s %s %s' % (record.last_name,
                                                 record.mother_name, record.first_name)
                record.name = display_name.upper()