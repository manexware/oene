# -*- coding: utf-8 -*-

import logging

from odoo import _, models, fields, api
from odoo.osv.expression import get_unaccent_wrapper

_logger = logging.getLogger(__name__)

PERSONAL_TYPE = [
    ('civil', 'Civil'),
    ('military', 'Military'),
]

class SieFaculty(models.Model):
    _name = 'sie.faculty'
    _inherit = 'res.partner'

    category = fields.Many2one('sie.category', u'Categoría', ondelete='restrict')
    pan_card = fields.Char('PAN Card', size=64)
    bank_acc_num = fields.Char('Bank Acc Number', size=64)
    type = fields.Selection(PERSONAL_TYPE, string='Tipo', default='civil', required=True)
    acronym = fields.Char('Acronimo', size=10)
    academic_title_id = fields.Many2one('sie.academic.title',string = u'Título')

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if args is None:
            args = []
        if name and operator in ('=', 'ilike', '=ilike', 'like', '=like'):
            self.check_access_rights('read')
            where_query = self._where_calc(args)
            self._apply_ir_rules(where_query, 'read')
            from_clause, where_clause, where_clause_params = where_query.get_sql()
            where_str = where_clause and (" WHERE %s AND " % where_clause) or ' WHERE '

            # search on the name of the contacts and of its company
            search_name = name
            if operator in ('ilike', 'like'):
                search_name = '%%%s%%' % name
            if operator in ('=ilike', '=like'):
                operator = operator[1:]

            unaccent = get_unaccent_wrapper(self.env.cr)

            query = """SELECT id
                             FROM sie_faculty
                          {where} ({email} {operator} {percent}
                               OR {display_name} {operator} {percent}
                               OR {reference} {operator} {percent})
                               -- don't panic, trust postgres bitmap
                         ORDER BY {display_name} {operator} {percent} desc,
                                  {display_name}
                        """.format(where=where_str,
                                   operator=operator,
                                   email=unaccent('email'),
                                   display_name=unaccent('display_name'),
                                   reference=unaccent('ref'),
                                   percent=unaccent('%s'))

            where_clause_params += [search_name] * 4
            if limit:
                query += ' limit %s'
                where_clause_params.append(limit)
            self.env.cr.execute(query, where_clause_params)
            partner_ids = map(lambda x: x[0], self.env.cr.fetchall())

            if partner_ids:
                return self.browse(partner_ids).name_get()
            else:
                return []
        return super(SieFaculty, self).name_search(name, args, operator=operator, limit=limit)


