# -*- coding: utf-8 -*-

from stdnum.ec import ruc, ci
from dateutil.relativedelta import relativedelta
import logging
import odoo
from odoo import _, models, fields, api, tools
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)

TYPE_CED_RUC = [('cedula', 'CEDULA'),
                ('ruc', 'RUC'),
                ('pasaporte', 'PASAPORTE')]

STUDY_LEVEL = [('first', 'Primer Nivel'),
               ('second', 'Segundo Nivel'),
               ('third', 'Tercer Nivel'),
               ('fourth', 'Cuarto Nivel'),
               ('fifth', 'Quinto Nivel')]

MARITAL_STATUS = [('single', 'Soltero'),
                  ('married', 'Casado'),
                  ('divorced', 'Divorciado'),
                  ('widower', 'Viudo'),
                  ('free_union', 'Union Libre'),
                  ]

BLOOD_GROUP = [('A+', 'A+'),
               ('B+', 'B+'),
               ('O+', 'O+'),
               ('AB+', 'AB+'),
               ('A-', 'A-'),
               ('B-', 'B-'),
               ('O-', 'O-'),
               ('AB-', 'AB-')]

PHYSICAL_EXONERATION = [('lactation', 'Lactancia'),
                        ('discapacity', 'Discapacidad'),]

GENDER = [
    ('male', _('Masculino')),
    ('female', _('Femenino')),
]
class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    name = fields.Char('Nombre',compute='_compute_name',store=True)
    full_name = fields.Char(compute='_compute_full_name',string="Nombre Completo", store=True)
    first_name = fields.Char("Primer Nombre")
    middle_name = fields.Char("Segundo Nombre")
    last_name = fields.Char("Primer Apellido")
    mother_name = fields.Char("Segundo Apellido")
    type_ced_ruc = fields.Selection(TYPE_CED_RUC, 'Tipo ID',default='cedula', track_visibility='onchange')
    ced_ruc = fields.Char(u'Cedula/ RUC',
                          help=u'Identificación o Registro Unico de Contribuyentes',track_visibility='onchange')
    title = fields.Many2one('sie.person.title', string=u'Título', ondelete='restrict')
    birth_date = fields.Date('Fecha de nacimiento')
    blood_group = fields.Selection(BLOOD_GROUP,'Tipo de sangre')
    nationality_id = fields.Many2one('res.country', string='Nacionalidad', ondelete='restrict')
    religion = fields.Many2one('sie.religion', 'Religion', ondelete='restrict')
    id_number = fields.Char('ID Card Number', size=64)
    grade_id = fields.Many2one('sie.grade', string=_('Grado'), ondelete='restrict')
    specialty_id = fields.Many2one('sie.specialty', string="Especialidad", ondelete='restrict')
    sub_specialty_id = fields.Many2one('sie.sub.specialty', string="Sub Especialidad",
                                      domain="[('specialty_id','=',specialty_id)]", ondelete='restrict')
    acronym = fields.Char(string='Acronimo')
    foreign = fields.Boolean(string='Extranjero?')
    age = fields.Integer(compute='_compute_age', string=_('Edad'), store=True)
    serial_navy = fields.Char(string="Serial Naval")
    birthplace_id = fields.Many2one('res.country',string="Lugar de Nacimiento")
    study_level = fields.Selection(STUDY_LEVEL,string=u"Eduación", default='first')
    marital_status = fields.Selection(MARITAL_STATUS,string="Estado Civil")
    gender = fields.Selection(GENDER, u'Genero')
    number_of_sons = fields.Integer(string="Numero de hijos")
    wifes_name = fields.Char(string="Nombre de esposo(a)")
    fathers_name = fields.Char(string="Nombre del padre")
    mothers_name = fields.Char(string="Nombre de la madre")
    physical_exoneration = fields.Selection(PHYSICAL_EXONERATION,string=u"Exoneración Física")
    conadis = fields.Char(string="CONADIS")
    conadis_percent = fields.Float(string="Porcentaje")
    observation_physical = fields.Char(u'Observación')
    emergency_contact = fields.Many2one(
        'res.partner', 'Contacto de emergencia', ondelete='restrict')
    email = fields.Char(copy= False)
    allocation_id = fields.Many2one('sie.allocation', string='Reparto de procedencia')


    _sql_constraints = [
        ('email_unique', 'UNIQUE(email)', u'Email debe ser único'),
        ('ced_ruc', 'UNIQUE(ced_ruc,type_ced_ruc)', u'Identificación debe ser única'),
        ('check_name', 'CHECK(1=1)','Contacto requiere un nombre'),
    ]

    _order = 'full_name'

    @api.multi
    @api.depends('grade_id', 'specialty_id', 'title', 'first_name', 'last_name', 'mother_name', 'middle_name')
    def _compute_name(self):
        for record in self:
            if record.grade_id and record.specialty_id:
                prefix = '%s-%s' % (record.grade_id.acronym, record.specialty_id.acronym)
            elif record.grade_id:
                prefix = '%s' % record.grade_id.acronym
            elif record.specialty_id:
                if record.title:
                    prefix = '%s' % record.title.acronym
                else:
                    prefix = ''
            else:
                if record.title:
                    prefix = '%s' % record.title.acronym
                else:
                    prefix = ''
            if len(prefix) > 0:
                if record.middle_name:
                    display_name = '%s %s %s %s %s' % (prefix, record.last_name, record.mother_name,record.first_name, record.middle_name)
                else:
                    display_name = '%s %s %s %s' % (prefix, record.last_name,
                                                    record.mother_name,record.first_name)
            else:
                if record.middle_name:
                    display_name = '%s %s %s %s' % (record.last_name, record.mother_name, record.first_name, record.middle_name)
                else:
                    display_name = '%s %s %s' % (record.last_name,
                                                 record.mother_name, record.first_name)
            record.name = display_name.upper()

    @api.one
    @api.depends('first_name', 'middle_name', 'last_name', 'mother_name')
    def _compute_full_name(self):
        for record in self:
            if self.first_name and record.last_name and record.mother_name:
                if record.middle_name:
                    display_name = '%s %s %s %s' % (record.last_name, record.mother_name,
                                                    record.first_name, record.middle_name)
                else:
                    display_name = '%s %s %s' % (record.last_name,
                                                 record.mother_name, record.first_name)
                record.full_name = display_name.upper()

    @api.multi
    @api.depends('birth_date')
    def _compute_age(self):
        for record in self:
            if record.birth_date:
                birth_date = fields.Datetime.from_string(record.birth_date)
                _logger.warning("Birthday: %s" % birth_date)
                today = fields.Datetime.from_string(fields.Datetime.now())
                # _logger.warning("Hoy: %s" % today)
                if today >= birth_date:
                    age = relativedelta(today, birth_date)
                    # _logger.warning(age)
                    record.age = age.years
                    # _logger.warning("Age: %s" % self.age)

    @api.multi
    @api.constrains('birth_date')
    def _check_birthdate(self):
        for record in self:
            if record.birth_date > fields.Date.today():
                raise ValidationError(_(
                    "Birth Date can't be greater than current date!"))

    @api.multi
    @api.constrains('ced_ruc', 'type_ced_ruc')
    def check_vat(self):
        for record in self:
            if record.type_ced_ruc and record.ced_ruc:
                if record.type_ced_ruc == 'cedula' and not ci.is_valid(record.ced_ruc):
                    raise ValidationError('CI [%s] no es valido !' % record.ced_ruc)
                elif record.type_ced_ruc == 'ruc' and not ruc.is_valid(record.ced_ruc):
                    raise ValidationError('RUC [%s] no es valido !' % record.ced_ruc)

    # @api.multi
    # @api.depends('name')
    # def name_get(self):
    #     result = []
    #     for record in self:
    #         result.append((record.id, '%s' % record.name))
    #     return result

    @api.model
    def _get_default_image(self, *args):
        img_path = odoo.modules.get_module_resource(
            'openedunav_core', 'static/src/img', 'army.png')
        with open(img_path, 'rb') as f:
            image = f.read()
        return tools.image_resize_image_big(image.encode('base64'))

    @api.depends('is_company', 'parent_id.commercial_partner_id')
    def _compute_commercial_partner(self):
        pass


