# -*- coding: utf-8 -*-

from odoo import _, models, fields, api


class SiePersonTitle(models.Model):
    _name = 'sie.person.title'

    acronym = fields.Char('Acronimo', size=10, required=True, search='_search_name')
    name = fields.Char('Nombre', size=96, required=True, search='_search_name')

    _sql_constraints = [
        ('acronym_uk', 'unique(acronym)', u'Acronimo debe ser único'),
        ('name_uk', 'unique(name)', u'Nombre debe ser único'),
    ]

    def _search_name(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        if self.name:
            if len(self.name) == 2:
                return [('acronym', operator, value)]
            else:
                return [('name', operator, value)]

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.search_count(
            [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
            new_acronym = u"Copy of {}".format(self.acronym)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)
            new_acronym = u"Copy of {} ({})".format(self.acronym, copied_count)
        default['name'] = new_name
        default['acronym'] = new_acronym
        return super(SiePersonTitle, self).copy(default)
