# -*- coding: utf-8 -*-

from dateutil.relativedelta import relativedelta
import logging

from odoo import _, models, fields, api
from odoo.osv.expression import get_unaccent_wrapper


_logger = logging.getLogger(__name__)


class SieStudent(models.Model):
    _name = 'sie.student'
    _inherit = 'res.partner'

    library_card = fields.Char('Library Card', size=64)
    guest = fields.Boolean('Invitado')
    inactive = fields.Boolean(string='Inactivo')
    admission_date = fields.Date(string='F.de ingreso Armada')
    academic_title_ids = fields.Many2many('sie.academic.title', string='Academic Titles')
    promotion_id = fields.Many2one(comodel_name='sie.promotion', string=u"Promoción del Estudiante", ondelete='restrict')
    force_years = fields.Integer(compute='_compute_force_years', string=u'Años en Fuerzas')

    @api.multi
    @api.depends('admission_date')
    def _compute_force_years(self):
        for record in self:
            if record.admission_date:
                admission_date = fields.Datetime.from_string(record.admission_date)
                _logger.warning("in Year: %s" % admission_date)
                today = fields.Datetime.from_string(fields.Datetime.now())
                _logger.warning("Hoy: %s" % today)
                if today >= admission_date:
                    calculate_age = relativedelta(today, admission_date)
                    # _logger.warning(calculate_age)
                    record.force_years = calculate_age.years
                    # _logger.warning("Force in year: %s " % self.force_years)

    @api.multi
    def check_data(self):
        self.env.user.notify_info('My information message')
        # db_model = self.env['base.external.dbsource'].search([('name','=','digedo')])
        # sql_query = 'select * from consultadigedo.v_tit_personal_mil_civ'
        # res = db_model.execute(sql_query)
        pass

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if args is None:
            args = []
        if name and operator in ('=', 'ilike', '=ilike', 'like', '=like'):
            self.check_access_rights('read')
            where_query = self._where_calc(args)
            self._apply_ir_rules(where_query, 'read')
            from_clause, where_clause, where_clause_params = where_query.get_sql()
            where_str = where_clause and (" WHERE %s AND " % where_clause) or ' WHERE '

            # search on the name of the contacts and of its company
            search_name = name
            if operator in ('ilike', 'like'):
                search_name = '%%%s%%' % name
            if operator in ('=ilike', '=like'):
                operator = operator[1:]

            unaccent = get_unaccent_wrapper(self.env.cr)

            query = """SELECT id
                                 FROM sie_student
                              {where} ({email} {operator} {percent}
                                   OR {display_name} {operator} {percent}
                                   OR {reference} {operator} {percent})
                                   -- don't panic, trust postgres bitmap
                             ORDER BY {display_name} {operator} {percent} desc,
                                      {display_name}
                            """.format(where=where_str,
                                       operator=operator,
                                       email=unaccent('email'),
                                       display_name=unaccent('display_name'),
                                       reference=unaccent('ref'),
                                       percent=unaccent('%s'))

            where_clause_params += [search_name] * 4
            if limit:
                query += ' limit %s'
                where_clause_params.append(limit)
            self.env.cr.execute(query, where_clause_params)
            partner_ids = map(lambda x: x[0], self.env.cr.fetchall())

            if partner_ids:
                return self.browse(partner_ids).name_get()
            else:
                return []
        return super(SieStudent, self).name_search(name, args, operator=operator, limit=limit)