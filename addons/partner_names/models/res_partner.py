# -*- coding: utf-8 -*-


import logging
_logger = logging.getLogger(__name__)
from odoo import api, fields, models, exceptions

class ResPartner(models.Model):
    """ This method will add `lastname` and `firstname`. `name` is a stored function field."""
    _inherit = 'res.partner'

    first_name = fields.Char("First name", required=True)
    middle_name = fields.Char("Middle name")
    last_name = fields.Char("Last name", required=True)
    mother_name = fields.Char("Mother name", required=True)
    name = fields.Char(
        compute="_compute_name",
        inverse="_inverse_name_after_cleaning_whitespace",
        required=False,
        store=True)

    @api.model
    def create(self, vals):
        """Override method to invert name at creation if unavailable."""
        context = dict(self.env.context)
        name = vals.get("name", context.get("default_name"))

        if name is not None:
            # Calculate the splitted fields
            inverted = self._get_inverse_name(
                self._get_whitespace_cleaned_name(name),
                vals.get("is_company",
                         self.default_get(["is_company"])["is_company"]))

            for key, value in inverted.iteritems():
                if not vals.get(key) or context.get("copy"):
                    vals[key] = value

            # Remove the combined fields
            if "name" in vals:
                del vals["name"]
            if "default_name" in context:
                del context["default_name"]

        return super(ResPartner, self.with_context(context)).create(vals)

    @api.multi
    def copy(self, default=None):
        """Override method to ensure partners are copied correctly."""
        return super(ResPartner, self.with_context(copy=True)).copy(default)

    @api.model
    def default_get(self, fields_list):
        """This method will invert `name` when getting default values."""
        result = super(ResPartner, self).default_get(fields_list)

        inverted = self._get_inverse_name(
            self._get_whitespace_cleaned_name(result.get("name", "")),
            result.get("is_company", False))

        for field in inverted.keys():
            if field in fields_list:
                result[field] = inverted.get(field)

        return result

    @api.model
    def _get_computed_name(self, last_name, first_name):
        """This method will compute the `name` from first and last names."""
        return u" ".join((p for p in (first_name, last_name) if p))

    @api.one
    @api.depends("first_name", "last_name")
    def _compute_name(self):
        """This method will re-compute and write the `name` field when it changes."""
        self.name = self._get_computed_name(self.last_name, self.first_name)

    @api.one
    def _inverse_name_after_cleaning_whitespace(self):
        # Remove unneeded whitespace
        clean = self._get_whitespace_cleaned_name(self.name)

        # Clean name avoiding infinite recursion
        if self.name != clean:
            self.name = clean

        # Save name in the real fields
        else:
            self._inverse_name()

    @api.model
    def _get_whitespace_cleaned_name(self, name):
        return u" ".join(name.split(None)) if name else name

    @api.model
    def _get_inverse_name(self, name, is_company=False):
        """ This method will compute the inverted name."""
        # Company name goes to the lastname
        if is_company or not name or name == 'Administrator':
            parts = [name or False, False]
            result = {"last_name": parts[0], "first_name": parts[1]}
        # Guess name splitting
        else:
            parts = name.strip().split(" ", 1)
            while len(parts) < 2:
                parts.append(False)
            result = {"last_name": parts[1], "first_name": parts[0]}
        return result

    @api.one
    def _inverse_name(self):
        """Try to revert the effect of :meth:`._compute_name`."""
        parts = self._get_inverse_name(self.name, self.is_company)
        self.last_name, self.first_name = parts["last_name"], parts["first_name"]

    @api.one
    @api.constrains("first_name", "last_name")
    def _check_name(self):
        if ((self.type == 'contact' or self.is_company) and not (self.first_name or self.last_name)):
            raise exceptions.UserError('Missing Last or First Name')

    @api.multi
    @api.onchange("first_name", "last_name")
    def _onchange_subnames(self):
        # Modify self's context without creating a new Environment.
        self.env.context = self.with_context(skip_onchange=True).env.context

    @api.multi
    @api.onchange("name")
    def _onchange_name(self):
        if self.env.context.get("skip_onchange"):
            # Do not skip next onchange
            self.env.context = (
                self.with_context(skip_onchange=False).env.context)
        else:
            self._inverse_name_after_cleaning_whitespace()

    @api.model
    def _install_partner_first_name(self):
        # Find records with empty firstname and lastname
        records = self.search([("first_name", "=", False),
                               ("last_name", "=", False)])

        # Force calculations there
        records._inverse_name()
        _logger.info("%d partners updated installing module.", len(records))

    _sql_constraints = [('check_name',"CHECK( 1=1 )",'Missing name')]
