# -*- coding: utf-8 -*-
{
    'name': 'First and Last Name for Partner',
    'summary': "Separate Name into First and Last Name for Partners",
    'version': '1.0',
    'author': "Manexware S.A.",
    'category': 'Extra Tools',
    'website':'https://www.manexware.com',
    'depends': ['base'],
    'data': [
        'views/res_partner.xml',
        'views/res_user.xml',
        'data/res_partner.yml',
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
}
