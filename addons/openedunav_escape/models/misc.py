# -*- coding: utf-8 -*

import subprocess
import re
import logging

from odoo import _

_logger = logging.getLogger(__name__)


def _check_verification_identification_id(identification):
    l = len(identification)
    if l == 10 or l == 13:  # check right length
        cp = int(identification[0:2])
        if 1 <= cp <= 22:  # check right state code
            tercer_dig = int(identification[2])
            if 0 <= tercer_dig < 6:  # number between 0 y 6
                if l == 10:
                    return _verification_identification_id(identification, 0)
                elif l == 13:
                    return _verification_identification_id(identification, 0) \
                           and identification[10:13] != '000'  # check last number not 000
            elif tercer_dig == 6:
                return _verification_identification_id(identification, 1)  # public society
            elif tercer_dig == 9:  # si es ruc
                return _verification_identification_id(identification, 2)  # public society
            else:
                return False
        else:
            return False
    else:
        return False


def _verification_identification_id(identification, num):
    total = 0
    unicodestring = identification
    identification_id = str(unicodestring).encode("utf-8")
    if num == 1:  # r.u.c. public
        base = 11
        d_ver = int(identification_id[8])
        _logger.warning("digit ver %d" % d_ver)
        multip = (3, 2, 7, 6, 5, 4, 3, 2)
        longitude = 8
    elif num == 2:  # r.u.c. legal and foreigners without id
        base = 11
        d_ver = int(identification_id[9])
        _logger.warning("digit ver %d" % d_ver)
        multip = (4, 3, 2, 7, 6, 5, 4, 3, 2)
        longitude = 9
    else:  # cedula y r.u.c persona natural
        base = 10
        d_ver = int(identification_id[9])  # check digit
        multip = (2, 1, 2, 1, 2, 1, 2, 1, 2)
        longitude = 9
    for i in range(0, longitude):
        a = int(identification_id[i])
        _logger.warning(a)
        b = multip[i]
        p = a * b
        if num == 0:
            total += p if p < 10 else int(str(p)[0]) + int(str(p)[1])
        else:
            total += p
    mod = total % base
    val = base - mod if mod != 0 else 0
    return val == d_ver


BLOOD_TYPE = [
    ('O-', 'O-'),
    ('O+', 'O+'),
    ('A-', 'A-'),
    ('A+', 'A+'),
    ('B-', 'B-'),
    ('B+', 'B+'),
    ('AB-', 'AB-'),
    ('AB+', 'AB+'),
]

GENDER = [
    ('male', _('Male')),
    ('female', _('Female')),
]

PERSONAL_TYPE = [
    ('civil', 'Civil'),
    ('military', 'Military'),
]

ROL = [
    ('director', _('Director')),
    ('planta', _('Oficial de Planta')),
    ('division', _('Oficial de Division')),

]

SCORE_NUMBER = [
    ('1', 'Note 1'),
    ('2', 'Note 2'),
    ('3', 'Note 3'),
    ('4', 'Note 4'),
    ('5', 'Note 5'),
    ('6', 'Note 6'),
    ('7', 'Note 7'),
    ('8', 'Note 8'),
    ('9', 'Note 9'),
    ('10', 'Note 10'),
    ('11', 'Note 11'),
    ('12', 'Note 12'),
    ('13', 'Note 13'),
    ('14', 'Note 14'),
    ('15', 'Note 15'),
    ('16', 'Note 16'),
    ('17', 'Note 17'),
]

YEAR = [
    ('1', '1'),
    ('2', '2'),
]

PERIOD = [
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
]

DURATION = [
    ('12', '12'),
    ('6', '6'),
    ('4', '4'),
    ('3', '3'),
]

MEASURE_UNIT = [
    ('time', _('Time')),
    ('number', _('Number')),
]

TIME_CONTROL = [
    ('between', _('Between')),
    ('exceed', _('Exceed')),
    ('not exceed', _('Not Exceed')),
]

SCORE_STATE = [
    ('draft', _('Draft')),
    ('published', _('Published')),
    ('for review', _('For review')),
    ('approved', _('Approved')),
]

CONTROL_STATE = [
    ('draft', _('Draft')),
    ('published', _('Published')),
    ('settled', _('Settled')),
]

COURSE_STATE = [
    ('planned', _('Planificado')),
    ('running', _('Ejecución')),
    ('finalized', _('Finalizado')),
]

ENROLLMENT_STATE = [
    ('enrolled', 'Enrolled'),
    ('retired', 'Retired'),
    ('failed', 'Failed'),
]

MONTHS = [
    ('a', 'January'),
    ('b', 'February'),
    ('c', 'March'),
    ('d', 'April'),
    ('e', 'May'),
    ('f', 'June'),
    ('g', 'July'),
    ('h', 'August'),
    ('i', 'September'),
    ('j', 'October'),
    ('k', 'November'),
    ('l', 'December'),
]
FORTNIGHT = [
    ('1', 'First'),
    ('2', 'Second')
]

ENROLLLMENT = [
    ('A', 'Alpha'),
    ('B', 'Bravo'),
    ('C', 'Charlie'),
    ('D', 'Delta'),
    ('E', 'Echo'),
    ('F', 'Foxtrot'),
    ('G', 'Golfo'),
    ('H', 'Hotel'),
    ('I', 'India'),
    ('J', 'Juliet'),
    ('K', 'Kilo'),
    ('L', 'Lima'),
    ('M', 'Mike'),
    ('N', 'November'),
    ('NN', u'Ñandu'),
    ('O', 'Oscar'),
    ('P', 'Papa'),
    ('Q', 'Quebeq'),
    ('R', 'Romeo'),
    ('S', 'Sierra'),
    ('T', 'Tango'),
    ('U', 'Uniform'),
    ('V', 'Victor'),
    ('W', 'Wisky'),
    ('X', 'X-Ray'),
    ('Y', 'Yankie'),
    ('Z', 'Zulu'),
]

time_value_pattern = re.compile(r'^[0-9]+,[0-5][0-9]$')
number_value_pattern = re.compile(r'^[0-9]+$')


def _get_ir_model_data_id(self, cr, uid, model, name):
    model_obj = self.pool.get('ir.model.data')
    result = model_obj._get_id(cr, uid, model, name)
    return model_obj.browse(cr, uid, result).res_id


def _get_ip():
    process = subprocess.Popen(["ifconfig"], stdout=subprocess.PIPE)
    ifc = process.communicate()
    pattern = re.compile(r'inet\s*\w*\S*:\s*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')
    res = pattern.findall(ifc[0])
    ip = ','.join(res)
    return ip
