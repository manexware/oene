# -*- coding: utf-8 -*


import time
import re
import logging

from odoo import models, fields, api, _


class SieSubjectContent(models.Model):
    _name = 'sie.subject.content'
    _description = 'Subject Content'


    name = fields.Char('Asignatura', size=96, required=True)
    subject_id = fields.Many2one('sie.subject')

    faculty_id = fields.Many2one('sie.faculty', string='Docente', required=True,
                                 ondelete='restrict')

    date_start = fields.Date('Fecha Inicio')
    date_end = fields.Date('Fecha Final')
    hours = fields.Integer('Horas Dictadas')
    score = fields.Float(u'Calificación')



    @api.multi
    @api.onchange('name')
    def do_stuff(self):
        for record in self:
            if record.name:
                record.name = record.name.upper()


