from odoo import models, fields, api
import odoo.addons.decimal_precision as dp
from odoo.exceptions import ValidationError
from misc import GENDER, MEASURE_UNIT, TIME_CONTROL, time_value_pattern, number_value_pattern

class SieProductivityCalParam(models.Model):
    _name = 'sie.productivity.cal.param'
    _description = 'Productivity Cal param'
    _rec_name = 'student_id'

    student_id = fields.Many2one(comodel_name='sie.student', string='Estudiante', ondelete='restrict',
                                 required=True, store=True)
    score = fields.Float(string='Puntaje', digits=(16, 2))
    productivity_cal_student_id = fields.Many2one(comodel_name='sie.productivity.cal.student', string='Productivity ID',
                                         ondelete='cascade')
                                         #string='Puntaje',
                                         #domain="[('productivity_cal_param_id','=',id)]")
    parameter_id = fields.Many2one('sie.matrix.parameter', string='Parameter', ondelete='restrict',
                                   required=True)

    _order = 'student_id'

    @api.multi
    @api.depends('score')
    def _compute_score(self):
        for record in self:
            record.nota += float(record.score)
            score = record.nota
            record.score = score

    # @api.multi
    # @api.depends('esprit_corps','honor','loyalty','honesty','cooperation')
    # def _compute_score(self):
    #     for record in self:
    #         record.nota = (record.esprit_corps)+(record.honor)+(record.loyalty)+(record.honesty)+(record.cooperation)
    #         score = (record.nota) / 5
    #     record.score = score