from odoo import models, fields, api
import odoo.addons.decimal_precision as dp
from odoo.exceptions import ValidationError
from misc import GENDER, MEASURE_UNIT, TIME_CONTROL, time_value_pattern, number_value_pattern

class SieAttitudeServiceStudent(models.Model):
    _name = 'sie.attitude.service.student'
    _description = 'Attitude Service student'
    _rec_name = 'student_id'

    student_id = fields.Many2one(comodel_name='sie.student', string='Estudiante', ondelete='restrict',
                                 required=True, store=True)
    score = fields.Float(string='Puntaje', digits=(16, 2))
    #score = fields.Float(string='Puntaje', compute='_compute_score', digits=(16, 2), store=True)
    attitude_service_id = fields.Many2one(comodel_name='sie.attitude.service', string='attitude_service ID',
                                         ondelete='cascade',
                                         #string='Puntaje',
                                         domain="[('attitude_service_student_id','=',id)]")
    #student_param_ids = fields.One2many(comodel_name='sie.productivity.cal.param', inverse_name='productivity_cal_student_id')
    full_name = fields.Char("Nombre", related='student_id.full_name', store=True)
    seq = fields.Integer('No.')

    #_order = 'student_id'
    _order = 'seq'

    @api.multi
    @api.depends('student_param_ids')
    def _compute_score(self):
        for record in self:
            score=0
            for param in record.student_param_ids:
                score += (param.score * param.parameter_id.coefficient)
            record.score = score

    # @api.multi
    # @api.depends('esprit_corps','honor','loyalty','honesty','cooperation')
    # def _compute_score(self):
    #     for record in self:
    #         record.nota = (record.esprit_corps)+(record.honor)+(record.loyalty)+(record.honesty)+(record.cooperation)
    #         score = (record.nota) / 5
    #     record.score = score