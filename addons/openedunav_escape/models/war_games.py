from odoo import _, models, fields


class SieWarGames(models.Model):
    _name = 'sie.war.games'

    name = fields.Char(string='Name', required=True, index=True)
    course_id = fields.Many2one('sie.course', string='Course', ondelete='restrict', required=True)
    faculty_ids = fields.Many2many('sie.faculty', string='Faculties', required=True)
    start_date = fields.Date(string='Start Date')
    end_date = fields.Date(string='End Date')

    _sql_constraints = [
        ('name_uk', 'unique(name)', _('Name must be unique'))
    ]
