from odoo import models, fields, api, _


class SieCourseProfessionalAttitude(models.Model):
    _name = 'sie.course.professional.attitude'
    _description = 'Student\'s Course Professional Attitude'
    _rec_name = 'faculty_id'

    name = fields.Char(string='Name', store=True)
    faculty_id = fields.Many2one('sie.faculty', string='Faculty',
                                 domain=" ['|',('director', '=', True), '|',('planta', '=', True),"
                                        "('division', '=', True)]",
                                 ondelete='restrict', required=True, store=True)
    rol = fields.Char(compute='_compute_rol', store=True)
    rol_name = fields.Char(compute='_compute_rol')
    director = fields.Boolean(string='Director')
    planta = fields.Boolean(string='Planta')
    division = fields.Boolean(string='Division')
    course_id = fields.Many2one('sie.course', ondelete='cascade')

    _order = 'name, faculty_id'

    _sql_constraints = [
        ('name_uk', 'unique(course_id,faculty_id)', _('Should not repeat faculty per course'))
    ]

    @api.one
    @api.depends('faculty_id', 'director', 'division', 'planta')
    def _compute_rol(self):
        if self.faculty_id:
            if self.director:
                self.rol = '026'
            if self.division:
                self.rol = '027'
            if self.planta:
                self.rol = '028'
            self.rol_name = self.env['sie.param.name'].search([('code', '=', self.rol)]).name

    @api.onchange('faculty_id')
    def _onchange_faculty_id(self):
        if self.faculty_id:
            self.planta = self.faculty_id.planta
            self.division = self.faculty_id.division
            self.director = self.faculty_id.director

    @api.onchange('director')
    def _onchange_director(self):
        if self.director:
            self.planta = False
            self.division = False
            self.rol = '026'

    @api.onchange('division')
    def _onchange_division(self):
        if self.division:
            self.director = False
            self.planta = False
            self.rol = '027'

    @api.onchange('planta')
    def _onchange_planta(self):
        if self.planta:
            self.division = False
            self.director = False
            self.rol = '028'

    @api.model
    def create(self, vals):
        if vals['director']:
            vals['planta'] = False
            vals['division'] = False
            vals['rol'] = '026'
        if vals['division']:
            vals['director'] = False
            vals['planta'] = False
            vals['rol'] = '027'
        if vals['planta']:
            vals['division'] = False
            vals['director'] = False
            vals['rol'] = '028'
        return super(SieCourseProfessionalAttitude, self).create(vals)

        # @api.multi
        # def write(self, vals):
        #     if vals['director']:
        #         vals['planta'] = False
        #         vals['division'] = False
        #         vals['rol'] = 'DIRECTOR'
        #     if vals['division']:
        #         vals['director'] = False
        #         vals['planta'] = False
        #         vals['rol'] = 'OFICIALES DIVISION'
        #     if vals['planta']:
        #         vals['division'] = False
        #         vals['director'] = False
        #         vals['rol'] = 'OFICIALES PLANTA'
        #     return super(SieCourseProfessionalAttitude, self).write(vals)
