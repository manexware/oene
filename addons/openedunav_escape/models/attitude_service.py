#Desarrollado por Ing. Ingrid Chilan
import time
import logging

from operator import attrgetter
from odoo import _, models, fields, api
from misc import CONTROL_STATE, SCORE_NUMBER
import icu

_logger = logging.getLogger(__name__)


class SieAttitudeService(models.Model):
    _name = 'sie.attitude.service'
    _description = 'Acttitude service'

    name = fields.Char(string='Nombre', compute='_compute_display_name', store=True)
    course_id = fields.Many2one(comodel_name='sie.course', string='Curso', domain="[('state', '=', 'running')]",
                               ondelete='restrict', required=True)
    enrollment_id = fields.Many2one(comodel_name='sie.enrollment', string='Division', ondelete='restrict',
                                    domain="[('course_id', '=', course_id)]")
    # student_ids = fields.One2many(comodel_name='sie.attitude_service.student',
    #                               string='Estudiante')
    student_ids = fields.One2many(comodel_name='sie.attitude.service.student', inverse_name='attitude_service_id',
                                  string='Estudiante')
    state = fields.Selection(CONTROL_STATE, 'State',default='draft')
    is_readonly = fields.Boolean(string='Is readonly?')
    score_number = fields.Selection(SCORE_NUMBER, string='No. Noa')

    # @api.multi
    # @api.depends('course_id', 'date')
    #
    #     for record in self:
    #         if record.course_id and record.date:
    #             create_date = time.strftime('%Y%m%d%H%M%S')
    #             name = '%s | %s ' % (record.course_id.name, create_date)
    #             record.name = name
    #
    @api.onchange('course_id')
    def onchange_course_id(self):
        students = []
        enrollment = self.env['sie.enrollment'].search([('course_id', '=', self.course_id.id)])
        student_ids = enrollment.student_ids
        seq = 0
        for student in student_ids:
            if not student.inactive:
                data = {
                    'name': student.ced_ruc,
                    'student_id': student.id,
                    'seq': seq
                }
                students.append(data)
        self.student_ids = students

    @api.multi
    def calcular(self):
        for record in self:
            attitude_service_ids = self.env['sie.attitude.service'].search([('course_id', '=', record.course_id.id)])
            #personal_appreciation_ids = self.env['sie.personal.appreciation'].search([('course_id', '=', record.course_id.id)])
            #physical_proof_ids = self.env['sie.physical_proof'].search([('course_id', '=', record.course_id.id)])
            #productivity_ids = self.env['sie.productivity'].search([('course_id', '=', record.course_id.id)])
            attitude_service_param = self.env['sie.attitude.service.param']
            for attitude_service in attitude_service_ids:
                for student in attitude_service.student_ids:
                    for student_cal in record.student_ids:
                        if student.student_id == student_cal.student_id:
                            # student_cal.score = student.score
                            values = {'student_id':student.student_id.id,'score':student.score,'parameter_id':attitude_service.parameter_id.id,'attitude_service_student_id':student_cal.id}
                            attitude_service_param.create(values)


    #
    # def publish(self):
    #     self.write({'state': 'published'})
    #     return True
    #
    # def settle(self):
    #     self.write({'state': 'settled'})
    #     return True

    # @api.multi
    # def load_tables(self):
    #     for record in self:
    #         if record.course_id:
    #             for student in record.student_ids:
    #                  physical_proof_table = self.env['sie.physical.proof.table'].search([('from_included','<=',student.student_id.age),
    #                                                                                     ('to_not_included','>=',student.student_id.age)],limit=1)
    #

                     #physical_proof_params = self.env['sie.physical.proof.param'].search([('table_id','=',physical_proof_table.id),
                                                                                       # ('gender','=',student.student_id.gender)])
                     # data_params = []
                     # for param in physical_proof_params:
                     #    data_params.append([0,0,{
                     #        'physical_proof_param_id': param.id,
                     #
                     # student.physical_proof_student_test_ids = data_params

    # @api.model
    # def create(self, values):
    #     e_obj = self.env['sie.enrollment'].browse(values.get('enrollment_id'))
    #     values['course_id'] = e_obj.course_id.id
    #     return super(SiePhysicalProof, self).create(values)

    # def copy(self, default=None):
    #     default = dict(default or {})
    #     default['state'] = 'draft'
    #     return super(SieProductivityCal, self).copy(default)

    # def write(self, values):
    #     record = self[0]
    #     e_obj = record.enrollment_id
    #     if e_obj.course_id:
    #         values['course_id'] = e_obj.course_id.id
    #     return super(SiePhysicalProof, self).write(values)

    # @api.multi
    # def unlink(self):
    #     unlink_ids = []
    #     for record in self:
    #         if record.state in ('settled'):
    #             raise models.Model.except_osv(_('Invalid Action!'), _('You can not delete an record which was settled'))
    #         if record.student_ids:
    #             for student in record.student_ids:
    #                 student.productivity_student_id.unlink()
    #     return super(SieProductivity, self).unlink()
    @api.multi
    def sort_by_name(self):
        for record in self:
             collator = icu.Collator.createInstance(icu.Locale('es'))
             # student_ids = sorted(record.student_ids,key=attrgetter('last_name','mother_name','first_name','middle_name'),cmp=collator.compare)
             student_ids = sorted(record.student_ids,key=attrgetter('full_name'),cmp=collator.compare)
             seq = 0
             for student in student_ids:
                 seq += 1
                 student.write({'seq':seq})

    @api.multi
    def sort_by_score(self):
        for record in self:
            student_ids = record.student_ids.sorted(
                key=attrgetter('score'), reverse=True)
            seq = 0
            for student in student_ids:
                seq += 1
                student.write({'seq': seq})
