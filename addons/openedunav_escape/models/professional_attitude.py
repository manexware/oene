import logging

from operator import attrgetter
from odoo import _, models, fields, api
from odoo.exceptions import ValidationError
from .misc import CONTROL_STATE, MONTHS, FORTNIGHT

_logger = logging.getLogger(__name__)


class SieProfessionalAttitude(models.Model):
    _name = 'sie.professional.attitude'
    _description = 'Professional Attitude'
    _rec_name = 'parameter_id'

    name = fields.Char(compute='_compute_name', store=True)
    notes = fields.Text(string='Notes')
    course_id = fields.Many2one('sie.course', string='Course', ondelete='restrict', required=True,
                                domain="[('state', '=', 'running'),'|',"
                                       "('course_professional.faculty_id.user_id','=',uid),"
                                       "'|',('subject_ids.faculty_id.user_id','=',uid),'|',"
                                       "('statistician.user_id','=',uid),('subject_ids.faculty_id.user_id','=',uid)]")
    subject_id = fields.Many2one('sie.subject', string='Subject', ondelete='restrict',
                                 domain="[('faculty_id', '=', faculty_id),('course_id', '=', course_id)]")
    student_ids = fields.One2many('sie.professional.attitude.student', inverse_name='score_id',
                                  string='Students', store=True)
    state = fields.Selection([('draft', _('Draft')), ('published', _('Published'))], string='State', default='draft')
    faculty_id = fields.Many2one('sie.faculty', string='Evaluator', store=True, ondelete='restrict')
    rol = fields.Char(compute='_compute_rol', string='Rol', store=True)
    month = fields.Selection(MONTHS, string='Month')
    fortnight = fields.Selection(FORTNIGHT, string='Fortnight')
    parameter_id = fields.Many2one('sie.matrix.parameter', ondelete='restrict', required=True)
    parameter_name = fields.Char(string='Parameter', compute='_compute_parameter_name')
    score_number = fields.Char(compute="_compute_score_number", string='Score', store=True)
    is_professor = fields.Boolean()

    _order = 'month, fortnight, score_number'

    _sql_constraints = [
        ('attitude_uk', 'unique(name)', _('Record must be unique')),
    ]

    @api.onchange('course_id')
    def onchange_course_id(self):
        students = []
        enrollment = self.env['sie.enrollment'].search([('name', '=', self.course_id.name)])
        student_ids = enrollment.student_ids.sorted(key=attrgetter('last_name_1', 'last_name_2'))
        for student in student_ids:
            if not student.inactive:
                data = {
                    'name': student.identification_id,
                    'student_id': student.id,
                }
                students.append(data)
        self.student_ids = students

    @api.one
    @api.depends('course_id')
    def _compute_rol(self):
        if self.course_id:
            faculty_id = self.env['sie.faculty'].search([('user_id', '=', self._uid)])
            course_professional_attitude = self.env['sie.course.professional.attitude']. \
                search([('course_id', '=', self.course_id.id), ('faculty_id', '=', faculty_id.id)])
            rol = ''
            if faculty_id.user_id.groups_id.filtered(lambda r: r.id == self.env.ref(
                    'openedunav_escape.group_sie_professional_attitude_professor').id).name:
                rol = '029'
                self.is_professor = True
            if faculty_id.user_id.groups_id.filtered(
                    lambda r: r.id == self.env.ref('openedunav_escape.group_sie_stats').id).name:
                rol = '030'
            if course_professional_attitude.director:
                rol = '026'
            if course_professional_attitude.planta:
                rol = '028'
            if course_professional_attitude.division:
                rol = '027'
            self.faculty_id = faculty_id
            self.rol = rol
            if rol == '':
                raise ValidationError('You must have asign a role')

    # @api.one
    @api.onchange('course_id')
    def _compute_parameter_id(self):
        if self.course_id and self.rol:
            res = {}
            domain = [('last_child', '=', True), ('parent_ref', 'like', '025'),
                      ('course_ref', '=', self.course_id.matrix_id.id)]
            param_name = []
            if self.rol == '029' or self.is_professor:
                param_name_id = self.env['sie.param.name']. \
                    search([('code', '=', '029')]).id
                # parameter_id = self.env['sie.matrix.parameter'].\
                #     search([('last_child', '=', True), ('parent_ref', 'like', '025'),
                #             ('param_name', '=', param_name.id),
                #             ('course_ref', '=', self.course_id.matrix_id.id)])
                param_name.append(int(param_name_id))
                # domain = domain + [('param_name', '=', param_name.id)]
            if self.rol == '030':
                param_name_id = self.env['sie.param.name']. \
                    search([('code', '=', '030')]).id
                # parameter_id = self.env['sie.matrix.parameter'].\
                #     search([('last_child', '=', True), ('parent_ref', 'like', '025'),
                #             ('param_name', '=', param_name.id),
                #             ('course_ref', '=', self.course_id.matrix_id.id)])
                param_name.append(int(param_name_id))
                # domain = domain + [('param_name', '=', param_name.id)]
            if self.rol == '026':
                param_name_id = self.env['sie.param.name']. \
                    search([('code', '=', '026')]).id
                # parameter_id = self.env['sie.matrix.parameter'].\
                #     search([('last_child', '=', True), ('parent_ref', 'like', '025'),
                #             ('param_name', '=', param_name.id),
                #             ('course_ref', '=', self.course_id.matrix_id.id)])
                param_name.append(int(param_name_id))
                # domain = domain + [('param_name', '=', param_name.id)]
            if self.rol == '028':
                param_name_id = self.env['sie.param.name']. \
                    search([('code', '=', '028')]).id
                # parameter_id = self.env['sie.matrix.parameter'].\
                #     search([('last_child', '=', True), ('parent_ref', 'like', '025'),
                #             ('param_name', '=', param_name.id),
                #             ('course_ref', '=', self.course_id.matrix_id.id)])
                param_name.append(int(param_name_id))
                # domain = domain + [('param_name', '=', param_name.id)]
            if self.rol == '027':
                param_name_id = self.env['sie.param.name']. \
                    search([('code', '=', '027')]).id
                # parameter_id = self.env['sie.matrix.parameter'].\
                #     search([('last_child', '=', True), ('parent_ref', 'like', '025'),
                #             ('param_name', '=', param_name.id),
                #             ('course_ref', '=', self.course_id.matrix_id.id)])
                param_name.append(int(param_name_id))
                # domain = domain + [('param_name', '=', param_name.id)]
            domain = domain + [('param_name', '=', param_name)]
            res['domain'] = {'parameter_id': domain}
            return res

    @api.one
    @api.depends('parameter_id')
    def _compute_parameter_name(self):
        if self.course_id:
            self.parameter_name = self.parameter_id.param_name.code

    @api.one
    def publish(self):
        self.state = 'published'
        return True

    @api.one
    def todraft(self):
        self.state = 'draft'

    @api.multi
    def unlink(self):
        unlink_ids = []
        for record in self:
            if record.state in 'published':
                raise ValidationError(_('No puedes borrar un registro publicado'))
            if record.rol == 'statistician':
                score = str(int(record.score_number) + 1)
                param_name = self.env['sie.param.name']. \
                    search([('code', '=', '030')])
                parameter_id = self.env['sie.matrix.parameter']. \
                    search([('last_child', '=', True), ('parent_ref', 'like', '025'),
                            ('param_name', '=', param_name.id),
                            ('course_ref', '=', self.course_id.matrix_id.id)])
                coevaluation = self.env['sie.professional.attitude']. \
                    search([('course_id', '=', record.course_id.id), ('parameter_id', '=', parameter_id.id),
                            ('score_number', '=', score)])
                if coevaluation:
                    raise ValidationError(_("Cannot delete record Score number %s") % str(int(record.score_number) + 1))

            unlink_ids.append(record.id)
        return super(SieProfessionalAttitude, self).unlink()

    @api.one
    @api.depends('course_id')
    def _compute_score_number(self):
        if self.course_id and self.rol == '030':
            param_name = self.env['sie.param.name']. \
                search([('code', '=', '030')])
            parameter_id = self.env['sie.matrix.parameter']. \
                search([('last_child', '=', True), ('parent_ref', 'like', '025'),
                        ('param_name', '=', param_name.id),
                        ('course_ref', '=', self.course_id.matrix_id.id)])
            score = len(self.env['sie.professional.attitude'].
                        search([('course_id', '=', self.course_id.id), ('parameter_id', '=', parameter_id.id),
                                ('name', '!=', self.name)]))
            self.score_number = score + 1

    @api.one
    @api.onchange('course_id')
    def _onchange_course(self):
        if self.course_id:
            self.parameter_id = None
            self.subject_id = None
            self.month = None
            self.fortnight = None

    @api.one
    @api.onchange('parameter_id')
    def _onchange_parameter(self):
        if self.parameter_id:
            self.month = None
            self.fortnight = None
            self.subject_id = None

    @api.one
    @api.onchange('course_id')
    def _onchange_course(self):
        if self.course_id:
            self.parameter_id = None
            self.subject_id = None

    @api.one
    @api.constrains('faculty_id')
    def _check_description(self):
        if self.parameter_id.param_name.code == '029':
            if self.faculty_id.type == 'civil':
                raise ValidationError("Profesor must be military")

    @api.one
    @api.depends('course_id', 'parameter_id', 'score_number')
    def _compute_name(self):
        name = '%s,%s,%s,%s,%s,%s,%s' % (self.course_id.id, self.parameter_id.id,
                                         self.month, self.fortnight,
                                         self.subject_id.id, self.score_number,
                                         self._uid)
        if self.rol == '027':
            name = '%s,%s,%s,%s,%s,%s' % (self.course_id.id, self.parameter_id.id,
                                          self.month, self.fortnight,
                                          self.subject_id.id, self.score_number)
        if self.parameter_id.param_name.code == '029':
            name = '%s,%s,%s,%s,%s,%s,%s' % (self.course_id.id, self.parameter_id.id,
                                             self.month, self.fortnight,
                                             self.subject_id.id, self.score_number,
                                             self._uid)
        self.name = name
