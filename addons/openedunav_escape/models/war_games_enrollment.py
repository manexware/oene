from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, Warning


class SieWarGamesEnrollment(models.Model):
    _name = 'sie.war.games.enrollment'
    _description = 'War Games Enrollment'

    name = fields.Char(string='Name', compute='_compute_fullname', store=True)
    course_id = fields.Many2one('sie.course', string='Course', ondelete='restrict', required=True)
    enrollment_id = fields.Many2one('sie.course', String='Enrollment', ondelete='restrict')
    war_games_id = fields.Many2one('sie.war.games', string='War Games', ondelete='restrict', required=True,
                                   domain="[('course_id', '=', course_id)]")
    student_ids = fields.One2many(comodel_name='sie.war.games.enrollment.student',
                                  inverse_name='enrollment_id', store=True, ondelete='cascade')
    # student_ids = fields.Many2many('sie.student',inverse_name='name', store=True,
    #                                domain="[('enrollment_id', '=' ,enrollment_id)]")
    # student_ids = fields.Many2many('sie.student',inverse_name='name', store=True,
    #                                domain=[('enrollment_id', '=' ,'enrollment_id')])
    no_of_students = fields.Integer(string='No of students', store=True)
    group_name = fields.Char(String='Group Name', required=True)

    _order = 'name, course_id'

    _sql_constraints = [
        ('name_uk', 'unique(name, course_id)', _('Classroom must be unique per course')),
    ]

    @api.multi
    @api.constrains('student_ids')
    def _check_student(self):
        if len(self.student_ids) == 0:
            raise ValidationError(_("Must enroll at least one student"))
        else:
            for record in self.student_ids:
                query = """ SELECT
                        sie_course.name as course,
                        sie_student.id as student,
                        count(*) as total
                    FROM
                        sie_enrollment_sie_student_rel,
                        public.sie_enrollment,
                        public.sie_course,
                        public.sie_student
                    WHERE
                        sie_enrollment_sie_student_rel.sie_enrollment_id = sie_enrollment.id AND
                        sie_enrollment_sie_student_rel.sie_student_id = sie_student.id AND
                        sie_enrollment.course_id = sie_course.id AND
                        sie_enrollment_sie_student_rel.sie_student_id = %s AND
                        sie_course.state = 'planned'
                    GROUP BY course, student
                """
                self._cr.execute(query, (record.id,))
                for course, student, total in self._cr.fetchall():
                    if total > 1:
                        raise Warning(student + ' ya se encuentra enrrolado en el curso ' + course)
            self.no_of_students = len(self.student_ids)

    @api.one
    @api.depends('course_id', 'war_games_id', 'group_name')
    def _compute_fullname(self):
        if self.course_id:
            fullname = '%s-%s-%s' % (self.war_games_id.name, self.course_id.name, self.group_name)
            self.name = fullname

    @api.onchange('war_games_id')
    def onchange_war_games_id(self):
        students = []
        count = 0
        enrollment = self.env['sie.enrollment'].search([('name', '=', self.course_id.name)])
        studends_register = self.env['sie.war.games.enrollment.student'] \
            .search([('war_games_id', '=', self.war_games_id.id)])
        for student in enrollment.student_ids:
            for student_register in studends_register:
                if student.identification_id == student_register.name:
                    count += 1
            if count == 0:
                data = {
                    'name': student.identification_id,
                    'student_id': student.id,
                    'course_id': self.course_id.id,
                    'war_games_id': self.war_games_id.id,
                }
                students.append(data)
            count = 0
        self.student_ids = students
