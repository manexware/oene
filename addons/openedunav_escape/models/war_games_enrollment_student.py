from odoo import models, fields, api
from odoo.exceptions import ValidationError


class SieWarGamesEnrollmentStudent(models.Model):
    _name = 'sie.war.games.enrollment.student'
    _description = 'Student\'s Score Professional Attitude'

    name = fields.Char(string='ID', store=True)
    student_id = fields.Many2one('sie.student', string='Student',
                                 ondelete='restrict', required=True, store=True)
    # enrollment_id = fields.Many2one('sie.war.games.enrollment',
    # string='Score Professional Attitude ID', ondelete='cascade')
    course_id = fields.Many2one('sie.course', string='Course', ondelete='restrict')
    war_games_id = fields.Many2one('sie.war.games', string='War Games', ondelete='restrict')
    enrollment_id = fields.Many2one('sie.war.games.enrollment', string='enrollment', ondelete='cascade')
    last_name_1 = fields.Char(compute='_compute_last_name', store=True)
    last_name_2 = fields.Char(compute='_compute_last_name', store=True)

    _order = 'last_name_1, last_name_2'

    @api.one
    @api.depends('student_id')
    def _compute_last_name(self):
        self.last_name_1 = self.student_id.last_name_1
        self.last_name_2 = self.student_id.last_name_2

    @api.multi
    def unlink(self):
        for obj in self:
            productivy = self.env['sie.integrator.product'].search(
                [('war_games_enrollment_id', '=', obj.enrollment_id.id)])
            if productivy:
                raise ValidationError("Student with grates")

        return super(SieWarGamesEnrollmentStudent, self).unlink()
