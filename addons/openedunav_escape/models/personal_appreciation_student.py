from odoo import models, fields, api
import odoo.addons.decimal_precision as dp
from odoo.exceptions import ValidationError
from misc import GENDER, MEASURE_UNIT, TIME_CONTROL, time_value_pattern, number_value_pattern

class SiePersonalAppreciationStudent(models.Model):
    _name = 'sie.personal.appreciation.student'
    _description = 'Student\'s Personal Appreciation Score'
    _rec_name = 'student_id'

    student_id = fields.Many2one(comodel_name='sie.student', string='Estudiante', ondelete='restrict',
                                 required=True, store=True)
    esprit_corps = fields.Float(string='Espiritu de cuerpo', digits=dp.get_precision('Score'), store = True )
    honor = fields.Float(string='Honor', digits=dp.get_precision('Score'), store=True)
    loyalty = fields.Float(string='Lealtad', digits=dp.get_precision('Score'), store=True)
    honesty = fields.Float(string='Honestidad', digits=dp.get_precision('Score'), store=True)
    cooperation = fields.Float(string='Cooperacion', digits=dp.get_precision('Score'), store=True)
    score = fields.Float(string='Nota', compute='_compute_score', digits=dp.get_precision('Score'), store=True)
    full_name = fields.Char("Nombre", related='student_id.full_name', store=True)
    seq = fields.Integer('No.')
    #score = fields.Float(string='Puntaje', digits=(16, 2))
    #score = fields.Float(string='Puntaje', compute='_compute_score', digits=(16,2), store=True)
    personal_appreciation_id = fields.Many2one(comodel_name='sie.personal.appreciation', string='Apreciacion Personal ID',
                                         ondelete='cascade',
                                         #string='Puntaje',
                                         domain="[('personal_appreciation_student_id','=',id)]")

    #_order = 'student_id'
    _order = 'seq'

    @api.multi
    @api.depends('esprit_corps','honor','loyalty','honesty','cooperation')
    def _compute_score(self):
        for record in self:
            if record.esprit_corps and record.honor and record.loyalty and record.honesty and record.cooperation:
                record.nota = (record.esprit_corps)+(record.honor)+(record.loyalty)+(record.honesty)+(record.cooperation)
                score = record.nota / 5
                record.score = score

    @api.multi
    @api.constrains('score')
    def validate_score(self):
        for record in self:
            if record.score < 0.0 or record.score > 20:
                raise ValidationError(record.student_id.name + ' revisar Nota ' + str(record.score))

    @api.multi
    @api.onchange('esprit_corps','honor','loyalty','honesty','cooperation')
    def validate_scores(self):
        for record in self:
            if record.esprit_corps < 0.0 or record.esprit_corps > 20:
                raise ValidationError(record.student_id.name + ' revisar Nota ' + str(record.esprit_corps))
            if record.honor < 0.0 or record.honor > 20:
                raise ValidationError(record.student_id.name + ' revisar Nota ' + str(record.honor))
            if record.loyalty < 0.0 or record.loyalty > 20:
                raise ValidationError(record.student_id.name + ' revisar Nota ' + str(record.loyalty))
            if record.honesty < 0.0 or record.honesty > 20:
                raise ValidationError(record.student_id.name + ' revisar Nota ' + str(record.honesty))
            if record.cooperation < 0.0 or record.cooperation > 20:
                raise ValidationError(record.student_id.name + ' revisar Nota ' + str(record.cooperation))