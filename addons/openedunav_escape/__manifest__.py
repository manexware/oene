{
    'name': 'OpenEduNav ESCAPE',
    'version': '1.0',
    'author': 'Manexware S.A.',
    'category': 'Education',
    'website': 'http://www.manexware.com',
    'summary': 'ESCAPE',
    'description': """
        Training Institutes.
        ================================

        You can manage:
        ---------------
        * Learning Techniques
        * Training Shafts
        * Subjects
        * Courses

        """,
    'css': [],
    'qweb': [],
    'summernote': [],
    'images': [],
    'depends': ['openedunav_core',
                'mail',
                'report_py3o'
                ],
    'demo': [

    ],
    'data': [
        'security/sie_security.xml',
        'security/ir.model.access.csv',
        'views/concept_views.xml',
        'views/promotion_course_views.xml',
        'views/subject_unit_views.xml',
        'views/subject_views.xml',
        'views/subject_content_views.xml',
        'views/param_name_views.xml',
        'views/matrix_parameter_views.xml',
        'views/matrix_views.xml',
        'views/course_views.xml',
        'views/enrollment_views.xml',
        'views/register_subject_views.xml',
        'views/register_directors_views.xml',
        'views/timekeeping_views.xml',
        'views/timekeeping_calc_views.xml',
        'views/war_games_views.xml',
        'views/war_games_enrollment_views.xml',
        'views/register_work_views.xml',
        'views/register_work_enrollment_views.xml',
        'views/register_seminary_views.xml',
        'views/score_views.xml',
        'views/integrator_product_views.xml',
        'views/professional_attitude_views.xml',
        'views/professional_attitude_statistician_views.xml',
        'views/professional_attitude_coevaluation_views.xml',
        'views/professional_attitude_professor_views.xml',
        'views/productivity_views.xml',
        'views/productivity_cal_views.xml',
        'views/faculty_views.xml',
        'views/course_name_views.xml',
        'views/score_statistician.xml',
        'views/attachment_views.xml',
        'views/student_views.xml',
        'views/personal_appreciation_view.xml',
        # 'views/attitude_service_views.xml',
        'views/course_score_views.xml',
        'data/concepts_data.xml',
        'data/promotion_course_data.xml',
        'data/matrix_parameter_name_data.xml',
        'data/matrix_data.xml',
        'data/matrix_param_data.xml',
        'data/course_name_data.xml',
        'report/personal_appreciation_act.xml',
        'report/productivity.xml',
        'report/productivity_report_final.xml',
        'report/attitude_service.xml',
        'report/student.xml',
        'menu/sie_menu.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}
