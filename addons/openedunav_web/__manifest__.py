# -*- coding: utf-8 -*-

{
    'name': 'OpenEduNav Web ',
    'description': 'Openedunav Website',
    'category': 'Website',
    "sequence": 3,
    'version': '3.0.0',
    'license': 'LGPL-3',
    'author': 'Manexware S.A.',
    'website': 'http://www.manexware.com',
    'data': [
        'views/assets.xml',
        'views/navbar_template.xml',
        'views/footer_template.xml',
        'views/homepage.xml',
        'views/feature_template.xml',
    ],
    'images': [
        'static/description/web_openeducat_banner.jpg',
    ],
    'depends': [
        'website',
    ],
}
