# -*- coding: utf-8 -*-
{
    'name': 'OpenEduNav QWeb Reports',
    'summary': """Special reports""",
    'author': 'Manexware, S.A.',
    'website': 'http://www.manexware.com',
    'category': 'Reports',
    'version': '0.1',
    'depends': [
        'report',
        'openedunav_aguena'
    ],
    'data': [
        'report/subject_layout.xml',
        'views/report_subject_syllabus.xml',
        'menu/main.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
